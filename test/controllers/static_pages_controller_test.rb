require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = 'LIMIT | 余命計算機'
  end

  test 'should get root' do
    get root_path(birthday: { year: '1930', month: '1', day: '1' }, sex: 'male')
    assert_response :success
    assert_select 'title', @base_title
  end

  test 'should get input' do
    get input_path
    assert_response :success
    assert_select 'title', "#{@base_title} | 入力画面"
  end

  test 'should get about' do
    get about_path
    assert_response :success
    assert_select 'title', "#{@base_title} | このサイトについて"
  end

  test 'check translater' do
    get root_path(birthday: { year: '1930', month: '1', day: '1' }, sex: 'male')
    assert_equal '男', assigns[:jpn_sex]
    get root_path(birthday: { year: '1930', month: '1', day: '1' }, sex: 'female')
    assert_equal '女', assigns[:jpn_sex]
    get root_path(birthday: { year: '1930', month: '1', day: '1' }, sex: 'not_human')
    assert_redirected_to input_path
  end
  test 'check NGdate' do
    get root_path(birthday: { year: '1930', month: '2', day: '1' }, sex: 'male')
    assert_equal Date.new(1930, 2, 1), assigns[:birthday]
    get root_path(birthday: { year: '1930', month: '2', day: '31' }, sex: 'male')
    assert_redirected_to input_path
  end
end

class StaticPagesHelperTest < ActionView::TestCase
  test 'my_age' do
    assert_equal 1, my_age_year(Date.new(2000, 1, 1), Date.new(2001, 1, 1).to_time)
    assert_raises(ArgumentError) { my_age_year(Date.new(2000, 2000, 2000)) }
    assert my_age_year(Date.new(2000, 1, 1)) >= 20
  end

  test 'rest_of_life' do
    assert_equal (81.7 * 365.2425 * 24 * 60 * 60).to_i, rest_of_life_sec(Date.new(2000, 1, 1), '女', Date.new(2000, 1, 1).to_time).to_i
    assert rest_of_life_sec(Date.new(1925, 1, 1), '男', Date.new(2020, 1, 1).to_time).between?(0, 3 * 365.2425 * 24 * 60 * 60)
    assert_raises(StandardError) { rest_of_life_sec(nil, nil) }
  end
end
