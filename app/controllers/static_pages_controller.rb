class IncorrectSexError < NoMethodError; end

class String
  # select caseで書けばよかった
  def to_japanese_sex
    if self == 'male'
      '男'
    elsif self == 'female'
      '女'
    else
      raise IncorrectSexError 'incorrect sex'
    end
  end
end

class StaticPagesController < ApplicationController
  def home
    # begin
    # メソッド頭なのでbeginは省略可
    @jpn_sex = params[:sex].to_japanese_sex
    @birthday = Date.new(params[:birthday][:year].to_i, params[:birthday][:month].to_i, params[:birthday][:day].to_i)
  rescue StandardError #=> e
    # puts e.message
    # puts e.backtrace.inspect
    redirect_to input_path
    # end # beginの対
  end

  # def input
  # end

  # def about
  # end
end
