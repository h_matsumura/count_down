module StaticPagesHelper
  def full_title(page_title = '')
    base_title = 'LIMIT | 余命計算機'
    if page_title.empty?
      base_title
    else
      base_title + ' | ' + page_title
    end
  end

  TOKYO_TIMEZONE = 9 * 60 * 60

  def my_age_year(birthday, arg_now = Time.now.getlocal(TOKYO_TIMEZONE))
    # 年齢表示機能用、整数部分だけで切り捨て
    # 日付を一度文字列→整数にして引き、年齢の整数部分を出す
    # raise 'type assert' unless arg_now.class == Time
    (arg_now.strftime('%Y%m%d').to_i - birthday.strftime('%Y%m%d').to_i) / 10_000
  end

  # https://www.mhlw.go.jp/toukei/saikin/hw/life/22th/index.html
  # 平成27年
  AVERAGE_ONE_YEAR_SECOND = 365.2425 * 24 * 60 * 60 # うるう年を計算すると一年は365.2425日

  def rest_of_life_sec(birthday, sex, arg_now = Time.now.getlocal(TOKYO_TIMEZONE))
    age_sec = arg_now - birthday.to_time
    raise 'incorrect arg_age' unless age_sec.between?(-2 * AVERAGE_ONE_YEAR_SECOND, 97 * AVERAGE_ONE_YEAR_SECOND)

    if sex == '男'
      (-0.767 * age_sec + 74.5 * AVERAGE_ONE_YEAR_SECOND).to_i
    elsif sex == '女'
      (-0.814 * age_sec + 81.7 * AVERAGE_ONE_YEAR_SECOND).to_i
    else
      raise 'incorrect sex'
    end
  end
end
